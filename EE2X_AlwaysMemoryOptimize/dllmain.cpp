// dllmain.cpp : Defines the entry point for the DLL application.
#include "pch.h"
#include <iostream>
#include <Windows.h>

DWORD WINAPI Thread() {
    FILE* fp;
    freopen_s(&fp, "CONOUT$", "w", stdout);
    AllocConsole();
    void* void_cdecl;
    while (true) {
        void_cdecl = malloc(500000);
        if (void_cdecl) {
            std::cout << "Allocated!!!" << std::endl;
            Sleep(300);
        }
    }
    return TRUE;
}
BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
        CreateThread(0, 0, (LPTHREAD_START_ROUTINE)Thread, 0, 0, 0);
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}

